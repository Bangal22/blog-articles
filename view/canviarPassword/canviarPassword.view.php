<?php /** @author Bangal;*/ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"  integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"rel="stylesheet">
    <link rel="stylesheet" href="../css/estils.css">
    <title>Recuperar</title>
</head> 

<a href="../../index.php"><span class="material-icons">arrow_back</span></a>
<body class="card-body-recuperar">
  <?php include "../../controller/canviarPassword/canviarPassword.controller.php"; ?>
  <div class="card-recuperar" > 
    <form method="post" action="<?php if(isset($_SESSION['correu'])) echo $_SESSION['correu']?>">
      <h1 class="h1-card-recuperar">Recuperar compte</h1>
      <div class="mb-3">
        <label for="nom" class="form-label">NickName: </label>
        <input type="text" class="form-control" name="nom" id="nom" aria-describedby="helpId" value="<?php if(isset($_SESSION['nom'])) echo $_SESSION['nom'] ?>" <?php if(($_SESSION['nom']) != "") echo "readonly"?> >
      </div>
      <div class="mb-3">
        <label for="telefon" class="form-label">Telefon: </label>
        <input type="telefon" class="form-control" name="telefon" aria-describedby="emailHelpId" value="<?php if(isset($_SESSION['telefon'])) echo $_SESSION['telefon']; ?>" <?php if(($_SESSION['telefon']) != "") echo "readonly"?> > 
      </div>  
      <div class="mb-3">
        <label for="correu" class="form-label">Correu: </label>
        <input type="email" class="form-control" name="correu" aria-describedby="emailHelpId" placeholder="Introdueix el correu amb el que vols recuperar el compte" value="<?php if(isset($_SESSION['correu2'])) echo $_SESSION['correu2'] ?>" <?php if(($_SESSION['correu2']) != "") echo "readonly"?>>
      </div>
     
      <div class="button-recuperar">
        <input type=<?php if(!$_SESSION['active']) echo "submit"; else echo "hidden"?> class="btn btn-primary btn-recuperar" name="Nova_Contrassenya" value=<?php if(!$_SESSION['active']) echo "Comprovar"; else echo $_SESSION['novaContrassenya']?> >
        <button type="submit" <?php if(!$_SESSION['active']) echo "style='display:none' "; ?> class="btn btn-lg btn-dark btn-block">Submit Form</button>
      </div>
      <div>
    </form>
  </div>  

</body>
</html>