<?php /*** @author Bangal */?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"  integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"rel="stylesheet">
    <link rel="stylesheet" href="view/css/estils.css">
    <title>Portal d'Articles</title>
</head>
<body>
    <!--MILLORA-->
    <nav class="navbar navbar-expand-sm navbar-light bg-light" id="navbar">
        <div class="container">
        <a class="navbar-brand" >Buscar article</a>
        <div class="collapse navbar-collapse" id="collapsibleNavId">
            <form class="d-flex my-2 my-lg-0" method="post">
                <input class="form-control me-sm-2" type="text" name="buscarArticle" placeholder="Introdueix l'id del article">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
            </form>
        </div>
        <div class="container-flow">
            <form method="post">
                <input type=<?php echo isset($_SESSION['tempsDeSessio']) ? "hidden" : "submit"?> name="login" class="btn btn-primary" value="Login">
                <input type=<?php echo isset($_SESSION['tempsDeSessio']) ? "hidden" : "submit"?> name="signUp" class="btn btn-success" value="Sign Up">   
            </form>
            <form method="post">
                <input type=<?php echo isset($_SESSION['tempsDeSessio']) ? "submit" : "hidden"?> name="insertarArticle" class="btn btn-outline-primary" value="Insertar Article">
                <input type=<?php echo isset($_SESSION['tempsDeSessio']) ? "submit" : "hidden"?> name="canviarDades" class="btn btn-outline-success" value="Canviar Dades">
                <input type=<?php echo isset($_SESSION['tempsDeSessio']) ? "submit" : "hidden"?> name="tancarSessio" class="btn btn-danger" value="Tancar Sessio"> 
            </form> 
        </div>                                      
      </div>
    </nav>

    
    <div class="container my-5"> 
        <h1>PORTAL D'ARTICLES</h1>
        <!--printem els 5 articles que ens retorna la SELECT amb el LIMIT que hem fet avanç -->  
        <?php for ($i=0; $i <sizeof($total); $i++) { ?>
            <div class="alert alert-light" role="alert" id = "<?php echo $total[$i]['id'];?>">
                <form method="post">    
                    <input type=<?php echo isset($_SESSION['tempsDeSessio']) ? "submit" : "hidden"?> class="btn-close" name= "borrar-btn" value="<?php echo $total[$i]['id'];?>"/>
                </form>
                <div class="flow">
                    <div class="form-group-article"> 
                        <strong style="color:green"> 
                            <!--MILLORA --> 
                            <!--Posem de color vermell el article que esta buscan -->    
                            Article numero: <?php 
                            if(isset($_SESSION['buscador'])){
                                if($_SESSION['buscador'] == intval($total[$i]['id'])) echo "<a style='color:red'>".$total[$i]['id']."</a>";
                                else echo $total[$i]['id']; 
                            }else echo $total[$i]['id'];
                            ?>
                        </strong> <?php echo $total[$i]['article'] ?> 
                    </div>
                    <div class = "img">
                        <div class = "cos-img">
                            <h4 class = "tito-imatge"><?php echo $totalImatges[$i]['titol']?></h4>
                            <img width="200" height="100" src="<?php echo $totalImatges[$i]['ruta']?>">
                        </div>     
                    </div>
                </div>
                <div class="see-more">
                    <form method="post">
                        <button name="see-complet" id="see-complet" value="<?php echo $total[$i]['id'];?>"  type="submit" class="btn btn-light" >Veure article complet</button>
                    </form>
                </div>
            </div>
        <?php } ?>
    </div>
    
    
    <nav aria-label="Page navigation example">
        <ul class="pagination">
            <!-- Si el $_GET['pagina'] és igual o menor que 1, llavors desabilitem el buto-->
            <li class="page-item <?php if($_GET['pagina'] <=1) echo 'disabled'; ?>">
                <a class="page-link" href="index.php?pagina=<?php echo $_GET['pagina']-1?>">Previous</a>
            </li>
  
            <!-- Si  el GET['pagina'] es igual que $i+1 llevors en el buto li posem active-->
            <?php for ($i=0; $i <$pagines; $i++) { ?>
                <li class="page-item <?php if($_GET['pagina'] == $i+1) echo "active" ?>"><a class="page-link" href="index.php?pagina=<?php echo $i+1 ?>"><?php echo $i+1 ?></a></li>
            <?php } ?>
            
            <!-- Si el $_GET['pagina'] és igual o major que la variable $pagines, llavors desabilitem el buto-->
            <li class="page-item <?php if($_GET['pagina'] >=$pagines) echo 'disabled'; ?>">
                <a class="page-link" href="index.php?pagina=<?php echo $_GET['pagina']+1?>">Next</a>
            </li>
        </ul>
    </nav>
    
</body>
</html>

