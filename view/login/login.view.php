<?php 
/**
 * @author Bangal Camara
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"  integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"rel="stylesheet">
    <link rel="stylesheet" href="../css/estils.css">
    <title>Login</title>
</head> 

<a href="../../index.php"><span class="material-icons">arrow_back</span></a>
<body class="card-body-login">
  <div class="card-login"> 
    <form method="post">
      <h1 class="h1-card-login">Login</h1>
      <div class="mb-3">
        <label for="nom" class="form-label">Nom: </label>
        <input type="text" class="form-control" name="nom" id="nom" aria-describedby="helpId" >
      </div>
      <div class="mb-3">
        <label for="password" class="form-label">Contrassenya: </label>
        <input type="password" class="form-control" name="contrassenya" aria-describedby="emailHelpId" >
      </div>  
      <div class="button-login">
        <input class="btn btn-primary btn-login" name="enviarLogin" value="Login" type="submit" >
      </div>
      <div>
    </form>

    <?php include '../../controller/login/login.controller.php'?>
  </div>  
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
  <path fill="#0d6efd" fill-opacity="1" d="M0,288L48,272C96,256,192,224,288,197.3C384,171,480,149,576,165.3C672,181,768,235,864,250.7C960,267,1056,245,1152,250.7C1248,256,1344,288,1392,304L1440,320L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"></path>
</svg>
</body>
</html>