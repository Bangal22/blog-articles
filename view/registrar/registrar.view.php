<?php /*** @author Bangal Camara */?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"  integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"rel="stylesheet">
    <link rel="stylesheet" href="../css/estils.css">
    <title>Registrar</title>
</head>


<body class="body-registrar">
    <?php include '../../controller/registrar/registrar.controller.php';?>
    <div class="card-body"> 
    <a href="../../index.php"><span class="material-icons">arrow_back</span></a>
        <form method="post">
            <h1 class="h1-card">Sign Up</h1>
            <div class="mb-3">
                <label for="nickName" class="form-label">Nickname: </label>
                <input type="text" class="form-control" name="nickName" id="nickName" aria-describedby="helpId" value="<?php if(isset($_POST['nickName'])) echo $_POST['nickName']?>">
            </div>
            <div class="mb-3">
                <label for="nom" class="form-label">Nom: </label>
                <input type="text" class="form-control" name="nom" id="nom" aria-describedby="helpId" value="<?php if(isset($_POST['nom'])) echo $_POST['nom']?>">
            </div>
            <div class="mb-3">
                <label for="cognom" class="form-label">Cognom: </label>
                <input type="text" class="form-control" name="cognom" id="cognom" aria-describedby="helpId" value="<?php if(isset($_POST['cognom'])) echo $_POST['cognom']?>">
            </div>
            <div class="mb-3">
                <label for="carrer" class="form-label">Carrer: </label>
                <input type="text" class="form-control" name="carrer" id="carrer" aria-describedby="helpId" value="<?php if(isset($_POST['carrer'])) echo $_POST['carrer']?>">
            </div>
            <div class="mb-3">
                <label for="telefon" class="form-label">Telefon: </label>
                <input type="text" class="form-control" name="telefon" id="telefon" aria-describedby="helpId" pattern="^\d{9}$" value="<?php if(isset($_POST['telefon'])) echo $_POST['telefon']?>">
            </div>
            <div class="mb-3">
                <label for="password1" class="form-label">Contrassenya: </label>
                <input type="password" class="form-control" name="password1" aria-describedby="emailHelpId" minlength="8" value="<?php if(isset($_POST['password1'])) echo $_POST['password1']?>">
            </div>
            <div class="mb-3">
                <label for="password2" class="form-label">Confirmar contrassenya: </label>
                <input type="password" class="form-control" name="password2"aria-describedby="emailHelpId" minlength="8" value="<?php if(isset($_POST['password2'])) echo $_POST['password2']?>">
            </div>     
            <div class="button-registrar">
                <input class="btn btn-success btn-registrar" name="enviar" value="Registrar" type="submit" >
            </div>
        </form>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
            <path fill="#157347" fill-opacity="1" d="M0,288L48,272C96,256,192,224,288,197.3C384,171,480,149,576,165.3C672,181,768,235,864,250.7C960,267,1056,245,1152,250.7C1248,256,1344,288,1392,304L1440,320L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"></path>
        </svg>

        <div>
            <?php if(!empty($resultat)) echo $resultat?>
        </div>
    </div>  
</body>
</html>