<?php /** @author Bangal;*/ ?>

<!doctype html>
<html lang="en">
  <head>
    <title></title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS v5.0.2 -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"  integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/estils.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"rel="stylesheet">
  </head>
  <body>
    <?php include '../../controller/canviarDades/canviarDades.controller.php'?>
    <div>
    <div class="card-body2">
      <a href="../../index.php"><span class="material-icons">arrow_back</span></a>
      <h1>CANVIRAR DADES</h1>
      <form method="post" enctype="multipart/form-data">
          <div class="mb-3">
              <label for="nick_name" class="form-label">Nik name: </label>
              <input class="form-control" value="<?php echo $dades[0][0];?>" minlength="5" maxlength="15" name="nick_name" id="nick_name" aria-describedby="helpId" rows="10" type="text">
          </div>
          <div class="mb-3">
              <label for="password" class="form-label">Contrasenya: </label>
              <input class="form-control" placeholder="Si deixes el camp en blanc, es matindra la mateixa contrasenya" name="password" id="password" aria-describedby="helpId" rows="10" type="password">
          </div>
          <div class="mb-3">
              <label for="nom" class="form-label">Nom: </label>
              <input class="form-control" value="<?php echo $dades[0][2];?>" name="nom" id="nom" aria-describedby="helpId" rows="10" type="text">
          </div><div class="mb-3">
              <label for="cognom" class="form-label">Cognom: </label>
              <input class="form-control" value="<?php echo $dades[0][3];?>" name="cognom" id="cognom" aria-describedby="helpId" rows="10" type="text">
          </div>
          <div class="mb-3">
              <label for="carrer" class="form-label">Carrer: </label>
              <input class="form-control" value="<?php echo $dades[0][4];?>" name="carrer" id="carrer" aria-describedby="helpId" rows="10" type="text">
          </div>
          <div class="mb-3">
              <label for="telefon" class="form-label">Telefon: </label>
              <input class="form-control" value="<?php echo $dades[0][5];?>" name="telefon" id="telefon" aria-describedby="helpId" rows="10" type="text">
          </div>
          <div class="button">
              <input class="btn btn-success" type="submit" name="actualitzar">
          </div>
      </form>
    </div>    
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320" class="svgV">
        <path fill="#157347" fill-opacity="1" d="M0,288L48,272C96,256,192,224,288,197.3C384,171,480,149,576,165.3C672,181,768,235,864,250.7C960,267,1056,245,1152,250.7C1248,256,1344,288,1392,304L1440,320L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"></path>
    </svg>
    <div>
  </body>
</html>


