<?php

session_start();
//Variables de session que utizem en diferents fitxers.
$_SESSION['intentsLogin'] = 0; //-> l'utilitzo en el fitxer login.php
$_SESSION['ContrasenyaJaCanviada'] = false; //-> l'utilitzo en el fitxer recuperarPassword.php

include 'controller/paginacio/paginacio.controller.php';
include 'controller/eliminarArticle/eliminarArticle.controller.php';
include 'controller/buscarArticle/buscarArticle.controller.php';

if(isset($_POST['see-complet'])){ 
    $_SESSION['articleID'] = $_POST['see-complet'];
    header('Location:view/articleComplet/articleComplet.view.php');
} 
if(isset($_POST['signUp'])) header('Location:view/registrar/registrar.view.php');
if(isset($_POST['login'])) header('Location:view/login/login.view.php');
if(isset($_POST['insertarArticle'])) header('Location:view/nouArticle/nouAtricle.view.php');
if(isset($_POST['tancarSessio'])){ session_unset(); session_destroy(); }
if(isset($_POST['canviarDades'])) header('Location:view/canviarDades/canviarDades.view.php');

include 'view/index/index.view.php';