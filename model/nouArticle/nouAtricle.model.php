<?php

/**
 * Funció que serveis per inserir un nou article.
 *
 * @param  mixed $article
 * @return void
 */
function insertarArticle($article){
    try{
        $connexio = new PDO('mysql:host=localhost;dbname=Activitat_2b_Bangal_Camara', 'root', '');
        $statement = $connexio->prepare("INSERT INTO paginacio (article) VALUES (?)");
        $statement->execute((array($article)));
    
    
        $statement2 = $connexio->prepare("SELECT id FROM paginacio ORDER BY id desc LIMIT 1");
        $statement2->execute();
        return $statement2->fetch();
    }catch(Exception $e){
        error ($e);
    }
}

/**
 * Funció que guarda la ruta de la imatge a la base de dades.
 *
 * @param  mixed $id
 * @param  mixed $upload
 * @param  mixed $titol
 * @return void
 */
function insertarFoto ($id,$upload,$titol){
    try{
        $connexio = new PDO('mysql:host=localhost;dbname=Activitat_2b_Bangal_Camara', 'root', '');
        $statement = $connexio->prepare("INSERT INTO fotos (id,ruta,titol) VALUES (?,?,?)");
        $statement->execute(array($id,$upload,$titol));
    }catch(Exception $e){
        error ($e);
    }
}
