<?php

/**
 * Funció per comprovar si un usuari ja existeix en la base de dades.
 *
 * @param  mixed $nom del usuari
 * @return boolean
 */
function consultarUsuariRegistrar ($nom){
    try{
        $connexio = new PDO('mysql:host=localhost;dbname=Activitat_2b_Bangal_Camara', 'root', '');
        $statement = $connexio->prepare("SELECT * FROM usuari WHERE nick_name=?");
        $statement->execute(array($nom));
        return sizeof($statement->fetchAll()) == 0 ? true : false;  
    }
    catch(Exception $e) {
        error ($e);
    }
}


/**
 * Funció per inserir un usuari
 *
 * @param  mixed $nom
 * @param  mixed $password
 * @return void
 */
function insertarUsuari ($nikname,$password,$nom,$cognom,$carrer,$telefon){
    try{
         $connexio = new PDO('mysql:host=localhost;dbname=Activitat_2b_Bangal_Camara', 'root', '');
         $statement = $connexio->prepare("INSERT INTO usuari (nick_name,password,nom,cognom,carrer,telefon) VALUES (?,?,?,?,?,?)");
         $statement->execute(array($nikname,$password,$nom,$cognom,$carrer,$telefon)); 
    }catch(Exception $e){
         error ($e);
     }
   
 
 }