<?php

/**
 * Funció que retorna el número d'articles.
 *
 */
function numeroDeArticles (){
    try{
        $connexio = new PDO('mysql:host=localhost;dbname=Activitat_2b_Bangal_Camara', 'root', '');
        $statement = $connexio->prepare("SELECT article,id FROM paginacio");
        $statement->execute();
        $articles = $statement->fetchAll();
        return $articles;
    }catch(Exception $e){
        error ($e);
    }
}

/**
 * Funció que serveix per assignar el missatge d'error que ha tingut el servidor a les variables de sessió.
 *
 * @param  mixed $error
 * @param  mixed $informacioDelError
 * @return void
 */
function errorDesconegut($error, $informacioDelError){
    session_start();
    $_SESSION['error'] = $error;
    $_SESSION['informacioDelError'] = $informacioDelError;
}

/**
 * Funció que ens retorna els articles que s'han de mostrar.
 *
 * @param  mixed $index
 * @param  mixed $articlePerPagines
 * 
 */
function articlesPerPagina ($index, $articlePerPagines){
    $connexio = new PDO('mysql:host=localhost;dbname=Activitat_2b_Bangal_Camara', 'root', '');
    $statement = $connexio->prepare("SELECT article,id FROM paginacio LIMIT $index,$articlePerPagines");
    $statement->execute();
    $total = $statement->fetchAll();
    return $total;
}

/**
 * Funció que ens retorna les imatges que s'han de mostrar.
 *
 * @param  mixed $index
 * @param  mixed $articlePerPagines
 * @return void
 */
function imatgesPerPagines($index, $articlePerPagines){
    try{
        $connexio = new PDO('mysql:host=localhost;dbname=Activitat_2b_Bangal_Camara', 'root', '');
        $statement = $connexio->prepare("SELECT id,ruta,titol FROM fotos LIMIT $index,$articlePerPagines");
        $statement->execute();
        $total = $statement->fetchAll();
        return $total;
    }catch(Exception $e){
        error ($e);
    }
}

/**
 * error
 *
 * @param  mixed $e
 * @return void
 */
function error ($e){
    header("Location:errors.php");
    errorDesconegut("Error amb la connexio amb la base de dades", $e->getMessage());
}