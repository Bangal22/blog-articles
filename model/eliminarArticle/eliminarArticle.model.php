<?php

/**
 * Funció que serveix per eliminar l'article que es vol eliminar i fer una redirecció de la pàgina principal.
 *
 * @param  mixed $valorBtn
 * @return void
 */
function eliminar($valorBtn){
    eliminarArticles(intval($valorBtn));
    header('Location:index.php?pagina='.$_GET['pagina']);
}

/**
 * Funció que elimina articles de la base de dades.
 *
 * @param  mixed $idArticle
 */
function eliminarArticles($idArticle){
    try{
        $connexio = new PDO('mysql:host=localhost;dbname=Activitat_2b_Bangal_Camara', 'root', '');
        $statement = $connexio->prepare("DELETE FROM paginacio WHERE id=?");
        $statement->execute(array($idArticle));
    
        $statement2 = $connexio->prepare("DELETE FROM fotos WHERE id=?");
        $statement2->execute(array($idArticle));
    }catch(Exception $e){
        error ($e);
    }
}