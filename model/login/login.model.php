<?php

/**
 * Funció que comprova si els camps introduïts no tenen injecció de codi i que no estiguin buits.
 *
 * @return void Retorna una cadena buida o un o més missatges d'error.
 */
function comprovarCampsBuits(){
    $valorARetornar = "";
    foreach ($_POST as $key => $value){
        if($key!='enviar'){
            $valorVariable = filter_var($_POST[$key], FILTER_SANITIZE_STRING);
            if($valorVariable == "" && $key != "password") $valorARetornar .= missatgeDeError("El camp <strong>$key</strong> està buit", "Error");
            else $_POST[$key] = $valorVariable;
        }
    }
    return $valorARetornar;
}

/**
 * Funció per comprovar si un usuari no existeix en la base de dades.
 *
 * @param  mixed $nom del usuari
 * @param  mixed $password del usuari
 * @return boolean
 */
function consultarUsuariLogin($nom,$password){
    try{
        $connexio = new PDO('mysql:host=localhost;dbname=Activitat_2b_Bangal_Camara', 'root', '');
        $statement = $connexio->prepare("SELECT * FROM usuari WHERE nick_name=? AND password=?");
        $statement->execute(array($nom,$password));
        return sizeof($statement->fetchAll()) == 0 ? false : true;

    }catch(Exception $e){
        error ($e);
    }
}

