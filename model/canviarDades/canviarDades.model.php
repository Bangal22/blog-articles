<?php

/**
 * Funció que serveix per agafar les dades de l'usuari que acaba d'iniciar sessió i mostrar-les en els camps de modificació de dades.
 *
 * @param  mixed $nomUsuari
 * @return void
 */
function dadesUsuari ($nomUsuari){
    try{
        $connexio = new PDO('mysql:host=localhost;dbname=Activitat_2b_Bangal_Camara', 'root', '');
        $statement = $connexio->prepare("SELECT nick_name,password,nom,cognom,carrer,telefon FROM usuari WHERE nick_name = ? ");
        $statement->execute(array($nomUsuari));
        return $statement->fetchAll();
    }catch(Exception $e){
        error ($e);
    }

}





/**
 * Funció que serveix per modificar les dades de l'usuari que acaba d'iniciar sessió.
 *
 * @param  mixed $nickName
 * @param  mixed $password
 * @param  mixed $nom
 * @param  mixed $cognom
 * @param  mixed $carrer
 * @param  mixed $telefon
 * @return void
 */
function modificarDades($nickName, $password,$nom,$cognom,$carrer,$telefon){
    try{
        $connexio = new PDO('mysql:host=localhost;dbname=Activitat_2b_Bangal_Camara', 'root', '');
        $statement = $connexio->prepare("UPDATE usuari SET password=?, nom=?, cognom=?, carrer=?, telefon=? WHERE nick_name=?");
        $statement->execute(array($password, $nom,$cognom,$carrer,$telefon,$nickName));
    }catch(Exception $e){
        error ($e);
    }
}

