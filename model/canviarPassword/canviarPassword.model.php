<?php


/**
 * Funció que serveix per consultar les dades que s'introdueixen en el formulari per poder gestionar la pèrdua de la contrasenya.
 *
 * @param  mixed $nom
 * @param  mixed $telefon
 * @return void
 */
function consultarUsuariRecuperarCompte($nom,$telefon){
    try{
        $connexio = new PDO('mysql:host=localhost;dbname=Activitat_2b_Bangal_Camara', 'root', '');
        $statement = $connexio->prepare("SELECT * FROM usuari WHERE nick_name=? AND telefon=?");
        $statement->execute(array($nom,$telefon));
        return sizeof($statement->fetchAll()) == 0 ? false : true;
    }catch(Exception $e){
        error ($e);
    }
}


/**
 * Funció que serveix per canviar la contrasenya a l'hora de recuperar la contrasenya.
 *
 * @param  mixed $password
 * @param  mixed $nick_name
 * @return void
 */
function canviarPassword($password,$nick_name){
    try{
        $connexio = new PDO('mysql:host=localhost;dbname=Activitat_2b_Bangal_Camara', 'root', '');
        $statement = $connexio->prepare("UPDATE usuari SET password=? WHERE nick_name=?");
        $statement->execute(array($password,$nick_name));
    }catch(Exception $e){
        error ($e);
    }
}