<?php


if(isset($_POST['buscarArticle'])) buscarArticle($total,$articles,$_POST['buscarArticle']);


function buscarArticle($total,$articles,$numeroDeArticle){
    //Fem una select per saber cuants articles hi ha
    //la variable $idArticle es l'id que ens passen en la busqueda
    $idArticle = intval($numeroDeArticle);
    $contador = 1;
    $contadorPagina = 1;
    if(sizeof($total)!=0){
        for ($i=0; $i <sizeof($articles); $i++) { 
            //Si $articles[$i]['id'] és igual que $idArticle, llavors redireccionem a la pàgina on está aquell article.
            if($articles[$i]['id'] == $idArticle){
                header('Location:index.php?pagina='.$contadorPagina);
                $_SESSION['buscador'] = intval($_POST['buscarArticle']);
                break;
            }   
            $contador++;
            //Si contador es igual a 5, llevaors li sumem 1 a al contador de pàgines.
            if($contador == 5){
                $contadorPagina++; 
                $contador = 1;
            }
        }  
    }
}