<?php
/**
 * @author Bangal
 * 
 * Fitxer on gestionem el que l'usuari vol veure.
 * 
 * Nickname Usuari amdin : admin
 * Password: Administrador123
*/

session_start();

include '../../model/login/login.model.php';

$resultat = "";
if(isset ($_POST['enviarLogin'])){
    $resultat = comprovarCampsBuits();
    if($resultat == ""){
        $nom = $_POST['nom'];
        $password = hash('sha512',$_POST['contrassenya']);
        
        //Comprovem que l'usuari existeixi en la base de dades.
        if(consultarUsuariLogin($nom,$password)){
            session_start();
            $_SESSION['tempsDeSessio'] = time();
            $_SESSION['usuari'] = $nom;
            header('Location:../../index.php');
        }else{ 
            //Mostrem el missatge de que hi ha hagut un error.
            echo missatgeDeError('Usuari i/o contrassenya no valid', "Error");
            //incrementem el comptador;
            $_SESSION['intentsLogin'] = $_SESSION['intentsLogin'] + 1;
        }

        //Si l'usuari ha intentat iniciar sessió més de 2 vegades, llavors mostrem el missatge de que si vol recuperar el compte.
        if($_SESSION['intentsLogin'] > 2) echo missatgeDeError("T'has oblidat de la contrasenya? <a href='../../view/canviarPassword/canviarPassword.view.php'>Canvia-la!!</a>", "Suggeriment");
    }   
}

/**
 * Funció que serveix per mostrar els errors.
 *
 * @param String $valor
 * @return string retorna un div amb el missatge d'error
 */
function missatgeDeError ($valor,$missatge){
    return '
    <div class="alert alert-secondary alert-dismissible fade show" role="alert">
        <strong>'.$missatge.'! : </strong>'.$valor.'.</div>
    ';
}

include '../../view/login/resultatLogin.view.php';