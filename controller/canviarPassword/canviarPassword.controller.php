<?php
/**
 * @author Bangal
 * 
 * 
 * Fitxer que serveix per gestionar la pèrdua de la contrasenya.
 * Per poder enviar el correu amb la nova contrasenya, ho he fet gràcies a https://formsubmit.co/ .
 * fromsubmit.io és una pàgina que s'encarrega d'trametre correus a qualsevol persona mitjançant un formulari.
 * Per fer que el formulari funcionés per a qualsevol correu, he hagut de modificar una mica el funcionament del formulari.
 */

include '../../model/canviarPassword/canviarPassword.model.php';
session_start();

//Si la variable $_SESSION['active'] és igual a false, llavors totes les variables de session que estan dins del condicional estaran buides.
$_SESSION['active'] = false;
if(!$_SESSION['active']){ $_SESSION['correu'] = ""; $_SESSION['nom'] = ""; $_SESSION['telefon'] = ""; $_SESSION['correu2'] = ""; $_SESSION['novaContrassenya'] = ""; }


//Si la variable $_SESSION['ContrasenyaJaCanviada'] és igual a true, significa que la contrasenya ja ha sigut canviada,
//en cas contrari entra dins del condicional i comprovem que els camps no estiguin buits, que no tinguin injecció de codi
//i que en les variables $_POST nom i telèfon estiguin en la base de dades.
if(!$_SESSION['ContrasenyaJaCanviada']){
    if(isset($_POST['Nova_Contrassenya'])){
        $return = comprovarCampsBuits();
        if($return == ""){
            //Si l'usuari i el telèfon estan en la base de dades, llavors assignem les variables de session declarades abans els seus corresponents valors.
            if(consultarUsuariRecuperarCompte($_POST['nom'],$_POST['telefon'])){
                $_SESSION['nickname'] = $_POST['nom'];
                $_SESSION['correu'] = "https://formsubmit.co/".$_POST['correu'];
                $_SESSION['active'] = true;
                $_SESSION['nom'] = $_POST['nom'];
                $_SESSION['telefon'] = $_POST['telefon'];
                $_SESSION['correu2'] = $_POST['correu'];
                if($_SESSION['novaContrassenya'] == ""){
                    $_SESSION['novaContrassenya'] = generatePassword(20);
                }
                $password = hash('sha512', $_SESSION['novaContrassenya']);

                //Cridem a la funcio per cambiar la contrasenya en la base de dades.
                canviarPassword($password,$_POST['nom']);
                echo missatgeAccioRealitzada("Comprovat", "Aquest usuari existeix en la base de dades");
                $_SESSION['ContrasenyaJaCanviada'] = true;
            }else{
                echo missatgeDeError("L'usuari o el numero de telefon son incorrectes","Error");
                $_SESSION['nickname'] = "";
                $_SESSION['correu'] = "";
            }
        }
        else echo $return;
    }
}
/**
 * Funció que comprova si els camps introduïts no tenen injecció de codi i que no estiguin buits.
 *
 * @return void Retorna una cadena buida o un o més missatges d'error.
 */
function comprovarCampsBuits(){
    $valorARetornar = "";
    foreach ($_POST as $key => $value){
        if($key!='enviar'){
            $valorVariable = filter_var($_POST[$key], FILTER_SANITIZE_STRING);
            if($valorVariable == "" && $key != "password") $valorARetornar .= missatgeDeError("El camp <strong>$key</strong> està buit", "Error");
            else $_POST[$key] = $valorVariable;
        }
    }
    return $valorARetornar;
}

/**
 * Funció que serveix per generar contrasenyes aleatòries.
 *
 * @param  mixed $length
 * @return void
 */
function generatePassword($length){
    $key = "";
    $pattern = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $max = strlen($pattern)-1;
    for($i = 0; $i < $length; $i++){
        $key .= substr($pattern, mt_rand(0,$max), 1);
    }
    return $key;
}


/**
 * Retorna un missatge de què l'acció s'ha realitzat.
 *
 * @param  mixed $h4
 * @param  mixed $strong
 * @return void
 */
function missatgeAccioRealitzada ($h4,$strong){
    return '
    <div class="alert alert-success" role="alert">
      <h4 class="alert-heading">'.$h4.'!!</h4>
      <strong>Missatge: </strong>'.$strong.'.</div>
    </div>';
}
/**
 * Funció que serveix per mostrar els errors.
 *
 * @param String $valor
 * @return string retorna un div amb el missatge d'error
 */
function missatgeDeError ($valor,$missatge){
    return '
    <div class="alert alert-secondary alert-dismissible fade show" role="alert">
        <strong>'.$missatge.'! : </strong>'.$valor.'.</div>
    ';
}






