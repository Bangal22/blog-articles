<?php
/**
 * @author Bangal
 *
 * 
 * Fitxer on gestionem la paginació de la pràctica.
*/

include 'model/paginacio/paginacio.model.php';


$articles = numeroDeArticles ();
$articlePerPagines = 4;
$pagines = ceil(sizeof($articles) / $articlePerPagines);

//Si no hi ha GET, redirigim a la pàgina número 1
if(!$_GET) header('Location:index.php?pagina=1');

//Si el $_GET['pagina'] és numèric, fem un explode per comprovar que no sigui un decimal, 
//i si la llargada del array és major que 1, significa que el $_GET['pagina'] és un decimal i redirigim a la pàgina d'errors.
if(is_numeric($_GET['pagina'])){
    $punt = explode(".",$_GET['pagina']);
    if(sizeof($punt)>1 || $punt[0]==0){
        header('Location:errors.php');  
        errorDesconegut("Error de paginació", "No pots posar numeros decimals en la url");
    } 
}
else header('Location:index.php?pagina=1');

//La variable $index servirà per saber a partir de quin article hem de començar a mostrar els articles restants.
$index = ($_GET['pagina']-1) * $articlePerPagines;
//Si la pàgina no compleix aquestes dues condicions, llevors redirigeix a la pàgina d'error.
if($_GET['pagina']<0 || $_GET['pagina']>$pagines) header('Location:index.php?pagina=1');
  
 
//Fem una consulta per agafar 4 articles i imatges.
$total = articlesPerPagina ($index, $articlePerPagines);
$totalImatges = imatgesPerPagines ($index, $articlePerPagines);