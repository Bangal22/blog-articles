<?php
/**
 * @author Bangal
 * 
 * 
 * Fitxer on gestionem les rutes de les imatges, les extensions de les imatges i les guardem a la base de dades.
*/

include '../../model/nouArticle/nouAtricle.model.php';

$result = "";
if(!empty($_FILES['file']['name']) && !empty($_POST['textArticle'] && !empty($_POST['titolImatge']))){
    $type = $_FILES['file']['type'];
    if(comprovarExtensioDelArxiu($type)){
        //Comprovem que el fitxer estigui carregat.
        if(is_uploaded_file($_FILES['file']['tmp_name'])){   
            //En la variable $ruta guardarem la ruta de la carpeta on guardarem els fitxers.   
            $ruta = "../../Upload/";  
            //En la variable $nomFinal trèiem els espais que hi haguin en blanc.      
            $nomFinal = trim($_FILES['file']['name']);
            $nomFinal = mb_ereg_replace (" ", "", $nomFinal);    
            //En la variable $upload guardem la ruta definitiva on l'usuari podrà fer clic i descarregar els arxius.    
            $upload = $ruta . $nomFinal;
            $article = filter_var($_POST['textArticle'], FILTER_SANITIZE_STRING);
            //Fem un insert a la base de dades i li pasaem per parametre la ruta definitiva.
            $id = insertarArticle($article);
            $titol = $_POST['titolImatge'];
            insertarFoto($id[0],'Upload/'.$nomFinal,$titol); 

            //movem els fitxers a la carpeta que tenim creada.
            move_uploaded_file($_FILES['file']['tmp_name'], $upload);
            $result = missatgeAccioRealitzada ("Enviat", "Article insertat correctament");
        }
    }
    else $result = missatgeAccioDenegada(); 
}

/**
 * Funció que comprova l'extensió de l'arxiu.
 * Aquesta funció només accepta extensió jpg, png, jpeg i gif.
 * @param  mixed $type
 * @return void
 */
function comprovarExtensioDelArxiu($type){
    if($type == "image/jpeg" || $type == "image/png" || $type == "image/gif" || $type == "image/jpg"){
        return true;
    }
    return false;
}
/**
 * Retorna un missatge de què l'acció s'ha realitzat.
 *
 * @param  mixed $h4
 * @param  mixed $strong
 * @return void
 */
function missatgeAccioRealitzada ($h4,$strong){
    return '
    <div class="alert alert-success" role="alert">
      <h4 class="alert-heading">'.$h4.'!!</h4>
      <strong>Missatge: </strong>'.$strong.'.</div>
    </div>';
}

/**
 * Retorna un missatge de què l'acció no s'ha realitzat.
 *
 * @return void
 */
function missatgeAccioDenegada(){
    return'
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <h4 class="alert-heading">Error!!</h4>
      <strong>Missatge: </strong> El format de la image ha de ser jpeg, jpg, gif o png
    </div>
    ';
}

include '../../view/nouArticle/resultatNouArticle.view.php';
?>
