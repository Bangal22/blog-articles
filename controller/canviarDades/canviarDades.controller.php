<?php
/**
 * @author Bangal
 * 
 * 
 * Fitxer on gestionem els canvis que vol realitzar l'usuari en les seves dades.
*/

include '../../model/canviarDades/canviarDades.model.php';
session_start();

$dades = dadesUsuari ($_SESSION['usuari']); //->Variable on es guarda les dades del usuari que ha iniciat sessió.
$resultat = "";
$passActivat = true;
$resultat = "";


if(isset($_POST['actualitzar'])){
    $comprovar = false;
    $resultat = comprovarCampsBuits();
    if($resultat == ""){
        //Si el camp contrasenya no està buit, es comprova que la nova contrasenya compleixi amb els requisits, i si el camp està buit, llavors la contrasenya encara és la mateixa.
        if($_POST['password'] != ""){
            if(comprovarPassword($_POST['password'])) $password = hash('sha512',$_POST['password']);
            else{
                echo missatgeDeError("Contrasenya feble.</br>La contaseña a de contenir al menys una lletra majuscula, un numero i una longitud minima de 8 caracters", "Error");
                $passActivat = false;
            }
        } 
        else $password = $dades[0][1];
        
        /*
        Si la variable $passActivat és igual a true, significa que l'usuari ha realitzat canvis. 
        Llavors cridem a la funció modificarDades per guardar les noves dades i li mostrem un missatge de què s'han fet els canvis correctament.
        */ 
        if($passActivat){
            modificarDades($_POST['nick_name'], $password, $_POST['nom'],$_POST['cognom'],$_POST['carrer'],$_POST['telefon']);
            $resultat = missatgeAccioRealitzada("Canvis realitzats correctament", "Refresca el navegador per poder veure els canvis que has realitzat");
            echo $resultat;
        }
    }
    else echo $resultat;
}

/**
 * Funció que comprova si els camps introduïts no tenen injecció de codi i que no estiguin buits.
 *
 * @return void Retorna una cadena buida o un o més missatges d'error.
 */
function comprovarCampsBuits(){
    $valorARetornar = "";
    foreach ($_POST as $key => $value){
        if($key!='enviar'){
            $valorVariable = filter_var($_POST[$key], FILTER_SANITIZE_STRING);
            if($valorVariable == "" && $key != "password") $valorARetornar .= missatgeDeError("El camp <strong>$key</strong> està buit", "Error");
            else $_POST[$key] = $valorVariable;
        }
    }
    return $valorARetornar;
}
/**
 * Comprova que la contrasenya compleixi amb els requisits.
 *
 * @param String $password
 * @return boolean 
 */
function comprovarPassword($password){
    return preg_match("/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/", $password);   
}

/**
 * Funció que serveix per mostrar els errors.
 *
 * @param String $valor
 * @return string retorna un div amb el missatge d'error
 */
function missatgeDeError ($valor,$missatge){
    return '
    <div class="alert alert-secondary alert-dismissible fade show" role="alert">
        <strong>'.$missatge.'! : </strong>'.$valor.'.</div>
    ';
}
/**
 * Retorna un missatge de què l'acció s'ha realitzat.
 *
 * @param  mixed $h4
 * @param  mixed $strong
 * @return void
 */
function missatgeAccioRealitzada ($h4,$strong){
    return '
    <div class="alert alert-success" role="alert">
      <h4 class="alert-heading">'.$h4.'!!</h4>
      <strong>Missatge: </strong>'.$strong.'.</div>
    </div>';
}
