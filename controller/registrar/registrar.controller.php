<?php
/**
 * @author Bangal
 * 
 * Fitxer on gestionem el registre de nous usuaris i comprovem que les dades que s'introdueixen siguin correctes
 */

include '../../model/registrar/registrar.model.php';
session_start();

$resultat = "";
if(isset($_POST['enviar'])){
    //Si els camps no estan buits, comprovem que les contrasenyes siguin iguals.
    $resultat = comprovarCampsBuits();
    if($resultat==""){
        $password1 = hash('sha512', $_POST['password1']);
        $password2 = hash('sha512', $_POST['password2']);
        //Si les contrasenyes són iguals, comprovem que compleixi amb els requisits que es demanen.
        if($password1 == $password2){
            //Si la contrasenya no es feble, llavors comprovem que el nickName sigui l'unic en la base de dades.
            if(comprovarPassword($_POST['password1'])){
                //Si es compleix la condició, llavors guardem les dades en la base de dades, y activem el temps de sessió a 10 minuts.
                if(consultarUsuariRegistrar($_POST["nickName"])){
                    $_SESSION['tempsDeSessio'] = time();
                    $_SESSION['usuari'] = $_POST["nickName"];
                    insertarUsuari ( $_POST["nickName"], $password1, $_POST["nom"], $_POST["cognom"], $_POST["carrer"], $_POST["telefon"]);
                    header('Location:../../index.php');
                }
                else{
                    $resultat = missatgeDeError("Aquest nom d'usuari ja existeix .<br> <a href='login.php'>Vols iniciar sessio?</a>", "Suggeriment");
                    $_POST['nickName'] = "";
                }  
            }
            else{
                $resultat = missatgeDeError("Contrasenya feble.</br>La contaseña a de contenir al menys una lletra majuscula, un numero i una longitud minima de 8 caracters", "Error");
                $_POST['password1'] = "";
                $_POST['password2'] = "";
            } 
        }
        else {
            $resultat = missatgeDeError('Els password no són iguals', "Error");
            $_POST['password2'] = "";
        }
    }
    else $resultat;
}

/**
 * Funció que comprova si els camps introduïts no tenen injecció de codi i que no estiguin buits.
 *
 * @return void Retorna una cadena buida o un o més missatges d'error.
 */
function comprovarCampsBuits(){
    $valorARetornar = "";
    foreach ($_POST as $key => $value){
        if($key!='enviar'){
            $valorVariable = filter_var($_POST[$key], FILTER_SANITIZE_STRING);
            if($valorVariable == "" && $key != "password") $valorARetornar .= missatgeDeError("El camp <strong>$key</strong> està buit", "Error");
            else $_POST[$key] = $valorVariable;
        }
    }
    return $valorARetornar;
}
/**
 * Comprova que la contrasenya compleixi amb els requisits.
 *
 * @param String $password
 * @return boolean 
 */
function comprovarPassword($password){
    return preg_match("/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/", $password);   
}
/**
 * Funció que serveix per mostrar els errors.
 *
 * @param String $valor
 * @return string retorna un div amb el missatge d'error
 */
function missatgeDeError ($valor,$missatge){
    return '
    <div class="alert alert-secondary alert-dismissible fade show" role="alert">
        <strong>'.$missatge.'! : </strong>'.$valor.'.</div>
    ';
}
 
?>
